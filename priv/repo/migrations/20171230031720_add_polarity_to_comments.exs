defmodule CarlabsComments.Repo.Migrations.AddPolarityToComments do
  use Ecto.Migration

  def change do
    alter table(:comments) do
      add :polarity, :string
    end
  end
end
