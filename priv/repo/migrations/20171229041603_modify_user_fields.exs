defmodule CarlabsComments.Repo.Migrations.ModifyUserFields do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :nickname, :string
      add :avatar_url, :string
      remove :email
    end
  end
end
