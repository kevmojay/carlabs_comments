# Carlabs Comments

## Description

Prototype and experimental comment feature

## Requirements

* [Elm](https://guide.elm-lang.org/install.html)
* [Elixir](http://elixir-lang.github.io/install.html)
* [Phoenix](https://hexdocs.pm/phoenix/installation.html#content)
* [PostgreSQL](https://www.postgresql.org/download/)

## Install

* Clone this Repo
* Register a new [Github oAuth app](https://github.com/settings/applications/new)
* Run `mix deps.get`
* In the config folder, create a config.exs and dev.exs based on the example files
* Replace the XXXX's in config.exs with the Github oAuth app credentials
* Configure dev.exs file to your database credentials
* Run `mix ecto.migrate`
* Navigate to web/elm and run `elm package install`
* In the project root directory run `mix phx.server`
* In your browser navigate to `localhost:4000`

## Features

* Basic commenting
* Github authentication
* Github avatar associated with comments
* Phoenix sockets with Elm
* Live comment updates via sockets
* Color coordinated sentiment analysis (Green = Positive, Blue = Neutral, Red = Negative)

## Future Features

* Edit and Delete Comments
* "Like" Feature
* Comment Thread
* Improved UX/Design
* Comment Sorting
