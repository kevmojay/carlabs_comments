defmodule CarlabsComments.UserSocket do
  use Phoenix.Socket

  channel "comments:*", CarlabsComments.CommentsChannel

  transport :websocket, Phoenix.Transports.WebSocket

  def connect(params, socket) do
    {:ok, socket}
    # case Phoenix.Token.verify(socket, "key", token) do
    #   {:ok, user_id} ->
    #     {:ok, assign(socket, :user_id, user_id)}
    #   {:error, _error} ->
    #     :error
    # end
  end

  def id(_socket), do: nil
end
