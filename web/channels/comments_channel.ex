defmodule CarlabsComments.CommentsChannel do
  use CarlabsComments.Web, :channel

  alias CarlabsComments.{Comment}
  alias CarlabsComments.User
  alias CarlabsComments.Aylien

  def join("comments:main", %{"token" => token}, socket) do
    comments = Comment
      |> Repo.all()
      |> Repo.preload(:user)
  
    case Phoenix.Token.verify(socket, "key", token) do
        {:ok, user_id} ->
            user = Repo.get(User, user_id)
        
          {:ok, %{comments: comments}, assign(socket, :comments, comments) |> assign(:user_id, user_id) |> assign(:user, user)}
        {:error, _error} ->
          :error
    end
  end

  def handle_in(name, %{"content" => content}, socket) do
    nickname = socket.assigns.user.nickname
    avatar_url = socket.assigns.user.avatar_url
    polarity = sentiment_analysis_comment(content)
    IO.inspect polarity

    changeset = socket.assigns.user
    |> build_assoc(:comments, nickname: nickname, avatar_url: avatar_url)
    |> Comment.changeset(%{comment: content, polarity: polarity})

    
    case Repo.insert(changeset) do
      {:ok, comment} ->
        
        broadcast!(socket, "new:comment",
          comment |> Repo.preload(:user)
        )
        {:reply, :ok, socket}
      {:error, _reason} ->
        {:reply, {:error, %{errors: changeset}}, socket}
    end
  end

  def sentiment_analysis_comment(comment) do

    url = "sentiment?text=#{comment}"
      |> URI.encode()

    headers = ["X-AYLIEN-TextAPI-Application-Key": "d4216db907456045cf3ff1265eaab0d9", "X-AYLIEN-TextAPI-Application-ID": "51bc86f5", "Accept": "Application/json; Charset=utf-8"]
    response = Aylien.get!(url, headers).body[:polarity]
    response
  end
end