defmodule CarlabsComments.Router do
  use CarlabsComments.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug CarlabsComments.Plugs.SetUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", CarlabsComments do
    pipe_through :api

    resources "/comments", CommentController
  end

  scope "/auth", CarlabsComments do
    pipe_through :browser

    get "/signout", AuthController, :signout
    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
  end

  scope "/", CarlabsComments do
    pipe_through :browser # Use the default browser stack

    get "/*path", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", CarlabsComments do
  #   pipe_through :api
  # end
end
