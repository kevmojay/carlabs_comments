
defmodule CarlabsComments.CommentView do
  use CarlabsComments.Web, :view

  def render("index.json", %{comments: comments}) do
    comments
  end
  def render("create.json", %{comment: comment}), do: comment
  def render("error.json", %{changeset: changeset}), do: changeset



end