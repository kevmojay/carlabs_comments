defmodule CarlabsComments.Comment do
  use CarlabsComments.Web, :model

  @derive {Poison.Encoder, only: [:comment, :id, :user, :polarity]}
  schema "comments" do
    field :comment, :string
    field :polarity, :string
    belongs_to :user, CarlabsComments.User

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:comment])
    |> cast(params, [:polarity])
    |> validate_required([:comment])
  end
end