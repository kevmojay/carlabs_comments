defmodule CarlabsComments.User do
  use CarlabsComments.Web, :model

  @derive {Poison.Encoder, only: [:nickname, :avatar_url]}
  schema "users" do
    field :email, :string
    field :nickname, :string
    field :avatar_url, :string
    field :provider, :string
    field :token, :string
    has_many :comments, CarlabsComments.Comment

    timestamps()
  end

  def changeset(struct, params \\ %{} )  do
    struct
    |> cast(params, [:email, :provider, :token, :nickname, :avatar_url])
    |> validate_required([:email, :provider, :token])
  end
end