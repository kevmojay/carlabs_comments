defmodule CarlabsComments.Aylien do
  use HTTPoison.Base

  @expected_fields ~w(
    polarity subjectivity text polarity_confidence subjectivity_confidence
  )

  @endpoint "https://api.aylien.com/api/v1/"

  def process_url(url) do
    @endpoint <> url
  end

  def process_response_body(body) do
    body
    |> Poison.decode!
    |> Map.take(@expected_fields)
    |> Enum.map(fn({k, v}) -> {String.to_atom(k), v} end)
  end
end