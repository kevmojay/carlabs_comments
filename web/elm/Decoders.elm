module Decoders exposing (..)

import Json.Decode as JD exposing (..)
import Json.Decode.Pipeline exposing (decode, required, optional)
import Json.Decode.Extra exposing ((|:))
import Http
import Json.Encode
import Model exposing (..)


type alias CommentResponse =
    { comment : Maybe Comment
    , error : Maybe String
    }


type alias NewComment =
    { comment : String
    }


commentsListDecoder : JD.Decoder CommentsList
commentsListDecoder =
    succeed
        CommentsList
        |: JD.list commentDecoder


commentDecoder : JD.Decoder Comment
commentDecoder =
    succeed
        Comment
        |: (field "id" int)
        |: (field "comment" string)
        |: (field "user" decodeUser)
        |: (field "polarity" string)


decodeUser : JD.Decoder User
decodeUser =
    succeed
        User
        |: (field "avatar_url" string)
        |: (field "nickname" string)


encodeNewComment : String -> Http.Body
encodeNewComment newComment =
    Http.jsonBody <|
        Json.Encode.object
            [ ( "comment", Json.Encode.string newComment ) ]
