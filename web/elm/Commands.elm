module Commands exposing (..)

import Decoders exposing (commentsListDecoder, commentDecoder, encodeNewComment, commentDecoder)
import Http
import Model exposing (..)
import Task exposing (..)


fetch : Cmd Msg
fetch =
    let
        apiUrl =
            "/api/comments"

        request =
            Http.get apiUrl commentsListDecoder
    in
        Http.send FetchResult request


joinChannel : Cmd Msg
joinChannel =
    Task.perform (always JoinChannel) (Task.succeed ())


postNewComment : Model -> String -> Cmd Msg
postNewComment model newComment =
    let
        apiUrl =
            "/api/comments"

        request =
            Http.post apiUrl (encodeNewComment newComment) commentDecoder
    in
        Http.send AddCommentHttp request
