module Update exposing (..)

import Commands exposing (fetch, postNewComment, joinChannel)
import Model exposing (..)
import Navigation
import Decoders exposing (commentDecoder)
import Routing exposing (Route(..), parse, toPath)
import Phoenix.Socket
import Phoenix.Channel
import Phoenix.Push
import Json.Encode as JE
import Json.Decode as JD exposing (field)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FetchResult (Ok response) ->
            { model | commentsList = Success response } ! []

        FetchResult (Err error) ->
            { model | commentsList = Failure "Something went wrong..." } ! []

        HandleNewPage ->
            model ! [ fetch ]

        UrlChange location ->
            let
                currentRoute =
                    parse location
            in
                urlUpdate { model | route = currentRoute }

        NavigateTo route ->
            model ! [ Navigation.newUrl <| toPath route ]

        UpdateCommentField comment ->
            { model | comment = comment } ! []

        AddComment ->
            let
                newComment =
                    model.comment

                payload =
                    (JE.object [ ( "user", JE.string model.userToken ), ( "content", JE.string newComment ) ])

                push_ =
                    Phoenix.Push.init "new:comment" "comments:main"
                        |> Phoenix.Push.withPayload payload

                ( phxSocket, phxCmd ) =
                    Phoenix.Socket.push push_ model.phxSocket
            in
                ( { model | comment = "", phxSocket = phxSocket }, Cmd.map PhoenixMsg phxCmd )

        AddCommentHttp (Ok response) ->
            { model | commentsList = Success (addComment response (getCurrentComments model)) } ! []

        AddCommentHttp (Err error) ->
            { model | commentsList = Failure "Something went wrong..." } ! []

        PhoenixMsg msg ->
            let
                ( phxSocket, phxCmd ) =
                    Phoenix.Socket.update msg model.phxSocket
            in
                ( { model | phxSocket = phxSocket }
                , Cmd.map PhoenixMsg phxCmd
                )

        JoinChannel ->
            let
                channel =
                    Phoenix.Channel.init "comments:main"
                        |> Phoenix.Channel.withPayload (userToken model.userToken)

                ( phxSocket, phxCmd ) =
                    Phoenix.Socket.join channel model.phxSocket
            in
                ( { model | phxSocket = phxSocket }
                , Cmd.map PhoenixMsg phxCmd
                )

        ReceiveComment raw ->
            let
                decoded =
                    JD.decodeValue commentDecoder raw

                _ =
                    Debug.log "Test" (decoded)
            in
                case JD.decodeValue commentDecoder raw of
                    Ok comment ->
                        { model | commentsList = Success (addComment comment (getCurrentComments model)) } ! []

                    Err error ->
                        ( model, Cmd.none )


userToken : String -> JE.Value
userToken userToken =
    JE.object [ ( "token", JE.string userToken ) ]


addComment : Comment -> List Comment -> CommentsList
addComment comment commentsList =
    { comments = List.append commentsList [ comment ] }


getCurrentComments : Model -> List Comment
getCurrentComments model =
    case model.commentsList of
        NotRequested ->
            []

        Failure error ->
            []

        Success comments ->
            comments.comments


urlUpdate : Model -> ( Model, Cmd Msg )
urlUpdate model =
    case model.route of
        HomeIndexRoute ->
            case model.commentsList of
                NotRequested ->
                    model ! [ fetch, joinChannel ]

                _ ->
                    model ! [ joinChannel ]

        _ ->
            model ! [ joinChannel ]
