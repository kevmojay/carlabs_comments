module Comment.View exposing (..)

import Common.View exposing (warningMessage)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (..)
import Routing exposing (Route(..))


commentView : Comment -> ( String, Html Msg )
commentView comment =
    let
        commentText =
            comment.comment

        nickname =
            comment.user.nickname

        avatar_url =
            comment.user.avatar_url

        comment_polarity =
            case comment.polarity of
                "negative" ->
                    "red lighten-1"

                "neutral" ->
                    "blue lighten-1"

                "positive" ->
                    "green lighten-1"

                _ ->
                    ""
    in
        ( toString comment.id
        , li [ class ("collection-item avatar flow-text " ++ comment_polarity) ]
            [ img [ src avatar_url, class "circle responsive-img" ]
                []
            , text commentText
            ]
        )
