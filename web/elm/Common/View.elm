module Common.View exposing (warningMessage)

import Html exposing (..)
import Html.Attributes exposing (class)
import Html.Events exposing (..)
import Model exposing (Msg(..))
import Routing exposing (Route(..))


warningMessage : String -> String -> Html Msg
warningMessage iconClasses message =
    div
        [ class "warning" ]
        [ span
            [ class "fa-stack" ]
            [ i [ class iconClasses ] [] ]
        , h4
            []
            [ text message ]
        ]
