module CommentsList.View exposing (indexView)

import Common.View exposing (warningMessage)
import Comment.View exposing (commentView)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Keyed exposing (..)
import Model exposing (..)


indexView : Model -> Html Msg
indexView model =
    div
        [ id "home_index" ]
        (viewContent model)


viewContent : Model -> List (Html Msg)
viewContent model =
    case model.commentsList of
        NotRequested ->
            [ text "" ]

        Failure error ->
            [ warningMessage
                "fa fa-meh-o fa-stack-2x"
                error
            ]

        Success comments ->
            [ Html.form [ onSubmit AddComment ]
                [ div [ class "input-field col s12" ]
                    [ textarea [ id "commentInput", class "materialize-textarea", onInput UpdateCommentField, value model.comment ] []
                    , label [ for "commentInput" ] [ text "Comment" ]
                    , button [ class "black-text yellow accent-4 btn waves-effect waves-light", type_ "submit" ] [ text "Submit" ]
                    ]
                ]
            , div
                []
                [ commentsList model comments ]
            ]


commentsList : Model -> CommentsList -> Html Msg
commentsList model comments =
    comments.comments
        |> List.map commentView
        |> Html.Keyed.node "ul" [ class "collection" ]
