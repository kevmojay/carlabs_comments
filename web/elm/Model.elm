module Model exposing (..)

import Routing exposing (Route)
import Http
import Navigation
import Phoenix.Socket
import Json.Encode as JE


type Msg
    = FetchResult (Result Http.Error CommentsList)
    | HandleNewPage
    | UrlChange Navigation.Location
    | NavigateTo Route
    | UpdateCommentField String
    | AddComment
    | AddCommentHttp (Result Http.Error Comment)
    | PhoenixMsg (Phoenix.Socket.Msg Msg)
    | JoinChannel
    | ReceiveComment JE.Value


type alias Flags =
    { user : String }


type RemoteData e a
    = NotRequested
    | Failure e
    | Success a


type alias Model =
    { commentsList : RemoteData String CommentsList
    , route : Route
    , comment : String
    , phxSocket : Phoenix.Socket.Socket Msg
    , userToken : String
    }


type alias CommentsList =
    { comments : List Comment
    }


type alias Comment =
    { id : Int
    , comment : String
    , user : User
    , polarity : String
    }


type alias User =
    { avatar_url : String
    , nickname : String
    }


initialCommentsList : CommentsList
initialCommentsList =
    { comments = []
    }


initialModel : Flags -> Route -> Model
initialModel flags route =
    { commentsList = NotRequested
    , route = route
    , comment = ""
    , phxSocket =
        Phoenix.Socket.init "ws://localhost:4000/socket/websocket"
            |> Phoenix.Socket.on "new:comment" "comments:main" ReceiveComment
    , userToken = flags.user
    }
