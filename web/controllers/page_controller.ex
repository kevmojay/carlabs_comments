defmodule CarlabsComments.PageController do
  use CarlabsComments.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
