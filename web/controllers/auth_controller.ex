defmodule CarlabsComments.AuthController do
  use CarlabsComments.Web, :controller
  plug Ueberauth

  alias CarlabsComments.User

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, params) do
    user_params = %{token: auth.credentials.token, email: auth.info.email, nickname: auth.info.nickname, avatar_url: auth.info.urls.avatar_url, provider: "github"}

    changeset = User.changeset(%User{}, user_params)

    signin(conn, changeset)
  end

  defp signin(conn, changeset) do
    case insert_or_update_user(changeset) do
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> redirect(to: static_path(conn, "/"))
      {:error, _reason} ->
        conn 
          |> redirect(to: static_path(conn, "/"))
    end
  end

  def signout(conn, _params) do
    conn
    |> configure_session(drop: true)
    |> redirect(to: static_path(conn, "/"))
  end

  defp insert_or_update_user(changeset) do
    case Repo.get_by(User, email: changeset.changes.email) do
      nil ->
        Repo.insert(changeset)
      user ->
        {:ok, user}
    end
  end
end