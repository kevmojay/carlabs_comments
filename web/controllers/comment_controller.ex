defmodule CarlabsComments.CommentController do
  use CarlabsComments.Web, :controller

  alias CarlabsComments.Comment

  def index(conn, _params) do
   comments = 
    Repo.all(Comment)
      |> Repo.preload(:user)

   render conn, comments: comments
  end

  def create(conn, params) do
    changeset = conn.assigns.user
    |> build_assoc(:comments)
    |> Comment.changeset(params)
   # changeset = Comment.changeset(%Comment{}, params)

    case Repo.insert(changeset) do
      {:ok, comment} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", comment_path(conn, :show, comment))
        |> render("show.json", comment: comment)
      {:error, changeset} -> 
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", changeset: changeset)
    end
  end
end